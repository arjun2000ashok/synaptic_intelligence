# Synaptic Intelligence

* Simple Structrual Regularizer that can be computed online and implemented locally at each synapse
* Characterize the importance measure of an indiviual synapse/parameter as 
```math
\omega_k^{\mu}
```
where $`k`$ is the parameter, and $`\mu`$ is the task 
* This importance measure indicates its "importance" in solving the previous task(s)
* Changes to such parameters (possibly with a high value of importance --> check) are penalized to avoid old memories from being overwritten. 


In terms of visualizing the loss landscape:

* They show how to find a local minima that is suited for both task 1 and 2 (that has the least value of loss for both).
* This is done by determining the $`\omega`$ values for each parameter and penalising changes / not allowing it to change.
* Probably --> This "not allowing it to change" is done using a regularizing loss function that involves $`\omega`$


## How do they find this regularizing loss function?

* **Crux** is that there is a quadratic surrogate loss function that matches the actual loss function (mimics the descent trajectory of the original loss) of the previous task.

* The feat of successfull training lies in finding learning trajectories for which the endpoint lies close to the minimum of the loss **on all tasks**

* They characterize the trajectory of the training
* Every parameter update is $`\frac{\partial \theta_k}{\partial t}`$. This is the infinitesimal change to $`\theta`$ at time $`t`$.

* They derive the following formula:
```math
\int_{t^{\mu-1}}^{t^{\mu}} g(\theta(t))(\theta'(t))dt = -\sum_{k}^{} \omega_k^{\mu}
```

Here,

* The left side is: Change is loss over an entire trajectory through the parameter space, which is equal to path integral of the gradient vector field along the parameter trajectory from the initial point($`t_0`$) to the final point($`t_1`$).

* The right side is: $`\omega_k^{\mu}`$ means 
```math
{\color{red} -} \int_{t^{\mu-1}}^{t^{\mu}} g_{\color{red}k}(\theta(t))(\theta_{\color{red}k}'(t))dt
``` 
that is, it is the per-parameter contribution to the total loss over the entire trajectory.

It is this quantity that is specific to each synapse(parameter) **for a particular task**.

And 
```math
\omega_k^{\mu} = \frac{\partial L}{\partial \theta_k} * \frac{\partial \theta_k}{\partial t}
```





